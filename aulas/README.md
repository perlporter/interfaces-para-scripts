# Interfaces para scripts em shell

## Índice

### [Aula 1 - Introdução](aula-01-introducao.md)

- [1.1 - O que é uma interface](aula-01-introducao.md#1-1-o-que-%C3%A9-uma-interface)
- [1.2 - O shell como interface](aula-01-introducao.md#1-2-o-shell-como-interface)

  - [Fluxos de dados padrão](aula-01-introducao.md#fluxos-de-dados-padr%C3%A3o)
  - [Dispositivos são interfaces](aula-01-introducao.md#dispositivos-s%C3%A3o-interfaces)
  - [Processos como interfaces](aula-01-introducao.md#processos-como-interfaces)
  - [E o meu script com isso?](aula-01-introducao.md#e-o-meu-script-com-isso)

- [1.3 - Classes de interfaces para scripts](aula-01-introducao.md#1-3-classes-de-interfaces-para-scripts)

  - [Interface para a linha de comando (CLI)](aula-01-introducao.md#interface-para-a-linha-de-comando-cli)
  - [Interface com o usuário via terminal (TUI)](aula-01-introducao.md#interface-com-o-usu%C3%A1rio-via-terminal-tui)
  - [Interface gráfica com o usuário(GUI)](aula-01-introducao.md#interface-gr%C3%A1fica-com-o-usu%C3%A1rio-gui)

## Parte 1 - Interfaces CLI

### [Aula 2 - Recebendo dados pela linha de comando](aula-02-recebendo-argumentos.md)

- [2.1 - Parâmetros especiais](aula-02-recebendo-argumentos.md#2-1-par%C3%A2metros-especiais)

  - [Variáveis ou parâmetros?](aula-02-recebendo-argumentos.md#vari%C3%A1veis-ou-par%C3%A2metros)

- [2.2 - Parâmetros posicionais](aula-02-recebendo-argumentos.md#2-2-par%C3%A2metros-posicionais)

  - [O conceito de 'palavra'](aula-02-recebendo-argumentos.md#o-conceito-de-palavra)
  - [Regras de citação](aula-02-recebendo-argumentos.md#regras-de-cita%C3%A7%C3%A3o)

- [2.3 - Expandindo parâmetros](aula-02-recebendo-argumentos.md#2-3-expandindo-par%C3%A2metros)

  - [O comando 'set'](aula-02-recebendo-argumentos.md#o-comando-set)
  - [Expandindo argumentos](aula-02-recebendo-argumentos.md#expandindo-argumentos)
  - [Expandindo parâmetros com '@' e '*'](aula-02-recebendo-argumentos.md#expandindo-par%C3%A2metros-com-e)

- [2.4 - Expandindo argumentos em 'loops'](aula-02-recebendo-argumentos.md#2-4-expandindo-argumentos-em-loops)

  - [O loop 'for' no estilo C](aula-02-recebendo-argumentos.md#o-loop-for-no-estilo-c)
  - [Expandindo parâmetros com os loops 'while' e 'until'](aula-02-recebendo-argumentos.md#expandindo-par%C3%A2metros-com-os-loops-while-e-until)

