# Aula 2 - Recebendo dados pela linha de comando

Como vimos, nas interfaces CLI, toda interação possível com o script se dá pela linha de comando que resulta na sua invocação: em outras palavras, é pela linha de comando que nós passamos dados e instruções para o script.

Por exemplo, observe o script `nome.sh`, abaixo:

```
#!/usr/bin/env bash

echo "Seu nome é $1."
```

Apesar de simples, existem vários conceitos importantes aqui, dos quais, o que merece a nossa atenção no momento é a expansão de uma variável que está sendo representada com `$1`.

Veja que curioso, nós sabemos que o cifrão (`$`), quando precede um identificador válido de uma variável, indica a expansão dessa variável. Porém, apesar de nomes de variáveis não poderem começar com caracteres numéricos, lá está uma variável de nome `1`. Será que aprendemos errado ou se trata de uma condição especial?

Vamos testar:

```
:~$ 3=teste
bash: 3=teste: comando não encontrado
```

De fato, como `3` não é um nome válido, o shell sequer interpreta nossa linha de comando como uma atribuição de um valor a uma variável. Em vez disso, toda a sequência de caracteres é vista como uma palavra que o shell tenta interpretar, sem sucesso, como um comando, o que nos leva a concluir que, se a construção `$1` for válida, existe algo de especial nela.

## 2.1 - Parâmetros especiais

Na verdade, além de variáveis identificadas por números, existem diversas outras variáveis com nomes que nós não poderíamos utilizar, mas que o shell utiliza para registrar uma série de informações úteis: são os **parâmetros especiais**.

A tabela abaixo descreve esses parâmetros especiais:

| Parâmetro | Nome | Descrição |
|---|---|---|
| `$0`...`$n` | Parâmetros posicionais | Expandem cada uma das palavras passadas como argumentos em uma linha de comando segundo sua ordem de aparição. O primeiro parâmetro, `$0`, sempre receberá o nome do executável que deu início à sessão do shell. |
| `$#` | Quantidade de argumentos | Expande um inteiro correspondente à quantidade de argumentos passados para a sessão do shell, o que **não inclui** na contagem o parâmetro `$0`. |
| `$@` e `$*` | Todos os argumentos | Expande uma lista de todos os argumentos passados para a sessão do shell separados pelo primeiro caractere definido na variável `IFS`. Sem aspas, não há diferença na expansão resultante de ambos. Entre aspas, porém, a expansão de `$@` obedecerá às regras de citação aplicadas na definição dos argumentos. |
| `$_` | Último argumento do comando anterior | Expande o último parâmetro posicional do último comando executado: que pode ser, inclusive, o valor em `$0`. |
| `$-` | Parâmetros do shell | Expande todas as opções ativas (*flags*) passadas na inicialização do shell. |
| `$?` | Estado de saída | Expande o estado de saída do último comando executado. |
| `$$` | PID da sessão | Expande o identificador do processo (PID) da sessão corrente do shell. |

### Variáveis ou parâmetros?

É muito comum vermos a expansão dos identificadores acima serem chamados de *"variáveis especiais"*. Embora não seja de todo errado, trata-se de uma imprecisão que nos dá a oportunidade de falarmos sobre a diferença entre esses dois conceitos:

- Uma **variável** é um identificador associado a um dado que pode *variar* ao longo da execução do programa.
- Um **parâmetro** é o dado que um programa (ou uma sub-rotina) **espera receber** na sua invocação.
- Complementarmente, os **argumentos** são os dados **passados** para um programa (ou sub-rotina), no momento da sua invocação, .

Em muitas linguagens, quando queremos especificar os parâmetros esperados, nós definimos as variáveis que receberão os dados passados na invocação do programa ou de uma sub-rotina (uma função, por exemplo). No shell, isso não é possível, porque...

> O programa que será executado a partir da invocação dos nossos scripts, na verdade, **sempre será uma sessão do shell** e, consequentemente, toda passagem de argumentos será para o shell.

Sendo assim, tudo que podemos fazer é expandir os **parâmetros especiais** do shell, que sempre aparecerão nos nossos scripts e comandos prefixados pelo cifrão (`$`).

## 2.2 - Parâmetros posicionais

Voltando ao nosso script `nome.sh`:

```
#!/usr/bin/env bash

echo "Seu nome é $1."
```

Agora nós sabemos que `$1` é a expansão da primeira palavra passada na linha do comando que invocar o nosso script. Então, vamos tentar alguns nomes:

```
# Sem argumentos...

:~$ ./nome.sh
Seu nome é .   <-- Nada foi expandido.

# Com um argumento...

:~$ ./nome.sh João
Seu nome é João.   <-- A primeira palavra era 'João'.
```

### O conceito de 'palavra'

Numa linha de comando, alguns caracteres terão significado especial para o shell, como espaços, tabulações, quebras de linha (nos scripts), além de: `|`, `&`, `;`, `(`, `)`, `<` e `>`. Esses caracteres (ou *metacaracteres*) irão compor os chamados **operadores** do shell e, quando aparecerem na linha do comando, eles delimitarão **palavras**.

Portanto, para o shell...

> Uma palavra é qualquer sequência contínua de caracteres delimitada por um operador.

Objetivamente, nós podemos afirmar que **cada palavra na linha de comando que invoca um script será um argumento passado para esse script**.

### Regras de citação

Contudo, nós podemos mudar o contexto em que os caracteres são analisados pelo shell, fazendo com que, aqueles que tiverem, percam seu significado especial e sejam tratados como caracteres textuais comuns. Essa mudança de contexto é feita pelas citações (*quoting*, em inglês) através de:

| Citação | Descrição |
|---|---|
| Contra-barra (`\`) | Remove o significado especial do caractere que vier imediatamente em seguida. |
| Aspas simples (`'...'`) | Remove o significado especial de todos os caracteres entre elas, sem exceção. |
| Aspas duplas (`"..."`) | Remove o significado especial de todos os caracteres entre elas, exceto o acento grave ( `` ` `` ), o cifrão (`$`), a contra-barra (`\`) e, no modo interativo, a exclamação (`!`). |

Revendo o script `nome.sh` em ação, observe:

```
# Com mais de um argumento...

:~$ ./nome.sh João da Silva <-- São três palavras.
Seu nome é João.   <-- Só expande o primeiro parâmetro!

# Com o nome completo citado entre aspas...

:~$ ./nome.sh 'João da Silva' <-- É um argumento só!
Seu nome é João da Silva.   <-- Ainda é a primeira palavra.
```

Para efeito do nosso estudo de interfaces CLI, este é o ponto mais relevante no momento: como as citações afetam os argumentos que estão sendo passados na invocação do script e como o shell expandirá os parâmetros recebidos.

## 2.3 - Expandindo parâmetros

Para facilitar as demonstrações de como os parâmetros especiais funcionam, em vez de utilizarmos um script, nós trabalharemos no modo interativo alterando os parâmetros da sessão do shell com o comando interno `set`.

### O comando 'set'

O comando `set` é utilizado para definir opções de execução do shell, mas também pode definir e modificar valores de parâmetros posicionais.

> Para informações adicionais, consulte `help set`, porque nós vamos nos concentrar apenas na possibilidade de alterarmos os parâmetros posicionais da sessão corrente do shell.

O comando `set` permite o uso do argumento `--` (dois traços) para separar as definições de opções do shell de uma lista de palavras que serão tratadas como parâmetros posicionais: tudo que vier depois de `--` será tratado como parâmetro posicional do shell; se não houver nada depois de `--`, os parâmetros posicionais da sessão corrente do shell serão destruídos.

> Isso vale tanto para o modo interativo quanto para os nossos scripts.

### Expandindo argumentos

Observe o exemplo:

```
:~$ set -- João Maria Luis Carlos
```

Com este comando, nós definimos quatro palavras que serão passadas para a sessão do shell como argumentos. Internamente, o shell recebe esses argumentos e os associa aos parâmetros posicionais a partir de `1`:

```
:~$ echo $1
João
:~$ echo $2
Maria
:~$ echo $3
Luis
:~$ echo $4
Carlos
```

Mas também podemos expandir o nome do executável que deu início à sessão do shell:

```
:~$ echo $0
bash
```

O número de argumentos recebidos pode ser obtido com a expansão do parâmetro especial `#`:

```
:~$ echo $#
4
```

Também podemos expandir todos os argumentos de uma vez:

```
# Com o parâmetro especial '*'...
:~$ echo $*
João Maria Luis Carlos

# Com o parâmetro especial '@'...
:~$ echo $@
João Maria Luis Carlos
```

Alterando a citação de caracteres, a quantidade de argumentos muda:

```
:~$ set -- João Maria 'Luis Carlos'
:~$ echo $#
3
```

Bem como os dados associados a cada parâmetro posicional:

```
:~$ echo $1
João
:~$ echo $2
Maria
:~$ echo $3
Luis Carlos
:~$ echo $4
            <-- Não há mais um parâmetro '4'!
```

Expandindo o `*` ou o `@` como argumentos do comando `echo`, não teremos como saber onde começam ou terminam as palavras:

```
# Com o parâmetro especial '*'...
:~$ echo $*
João Maria Luis Carlos

# Com o parâmetro especial '@'...
:~$ echo $@
João Maria Luis Carlos
```

Neste caso, o comando `printf` pode ser mais útil:

```
# Inserindo uma quebra de linha depois de cada argumento...
:~$ printf '%s\n' $*
João
Maria
Luis
Carlos
```

Da forma que a expansão foi feita, não há diferença entre o `*` e o `@`:

```
# Inserindo uma quebra de linha depois de cada argumento...
:~$ printf '%s\n' $@
João
Maria
Luis
Carlos
```

A diferença aparece quando expandimos `*` ou `@` entre aspas duplas:

```
# O '*' expande uma lista de palavras que,
# entre aspas, tornam-se uma só palavra...

:~$ printf '%s\n' "$*"
João Maria Luis Carlos

# O '@' expande uma lista de palavras que,
# entre aspas, mantém a separação original...

:~$ printf '%s\n' "$@"
João
Maria
Luis Carlos
```

### Expandindo parâmetros com '@' e '*'

Dentre as diversas transformações possíveis através das expansões do shell, nós podemos extrair *substrings* em variáveis escalares e faixas de elementos em vetores. Em termos de implementação, o mecanismo que acessamos com os símbolos `@` e `*` são vetores. Porém, para efeito de expansões, nós os tratamos como variáveis escalares.

Observe:

```
# Não podemos expandir o '@' como um vetor...

:~$ echo ${@[2]}
bash: ${@[2]}: substituição incorreta

# Mas podemos obter o argumento de índice '2' com
# uma expansão típica de variáveis escalares...

:~$ echo ${@:2:1}
Maria

# Ou o terceiro argumento...

:~$ echo ${@:3:1}
Luis Carlos
```

O mais interessante, porém, é que as citações dos argumentos são mantidas mesmo quando expandimos o `*` desta forma:

```
:~$ echo ${*:1:1}
João
:~$ echo ${*:2:1}
Maria
:~$ echo ${*:3:1}
Luis Carlos
```

O parâmetro `0` também pode ser expandido assim:

```
:~$ echo ${*:0:1}
bash
:~$ echo ${*:0:1}
bash
```

> Para aprender mais sobre este tipo de expansão, estude o [tópico 7.11 do curso Shell GNU](https://codeberg.org/blau_araujo/o-shell-gnu/src/branch/main/aulas/aula-07.md#7-11-outras-expans%C3%B5es-de-par%C3%A2metros).

## 2.4 - Expandindo argumentos em 'loops'

Como vimos, nós podemos acessar individualmente os argumentos passados na invocação dos nossos scripts pela expansão do parâmetro posicional correspondente à sua ordem de aparição na linha do comando:

```
:~$ set -- a b 'c d' e
:~$ echo $2
b
:~$ echo $4
e
:~$ echo $3
c d
```

Também podemos expandir todos os argumentos de uma vez com os parâmetros especiais `@` e `*`:

```
:~$ set -- a b 'c d' e
:~$ echo $@
a b c d e
:~$ echo $*
a b c d e
```

Neste último caso, o resultado da expansão será uma **lista de palavras**: que é exatamente a forma comum de controle da estrutura de repetição `for`:

```
# Em uma linha...

for VAR in LISTA_DE_PALAVRAS; do COMANDOS; done

# Em várias linhas...

for VAR in LISTA_DE_PALAVRAS; do
    COMANDOS
done
```

Sendo assim, nós podemos expandir sequencialmente (e na ordem de aparição) cada um dos parâmetros posicionais:

```
:~$ set -- a b 'c d' e
:~$ for arg in $@; do echo $arg; done
a
b
c
d
e
```

Como o `@` foi expandido sem aspas, as citações dos argumentos foram ignoradas, o que resulta numa lista contendo cinco palavras distintas e, consequentemente, cinco ciclos de execução do bloco de comandos.

Se expandido entre aspas, porém, o `@` expande os parâmetros respeitando as citações aplicadas aos argumentos na linha do comando:

```
:~$ set -- a b 'c d' e
:~$ for arg in "$@"; do echo $arg; done
a
b
c d
e
```

Por outro lado, a expansão do parâmetro especial `*` entre aspas resulta em apenas uma palavra contendo todos os argumentos passados para o script e, portanto, na execução de apenas um ciclo do loop `for`:

```
:~$ set -- a b 'c d' e
:~$ for arg in "$*"; do echo $arg; done
a b c d e
```

No Bash, a expansão do `@` entre aspas é tão comum (e útil) que, quando for este o caso, a sintaxe do `for` pode ser simplificada com a omissão da parte `in "$@"`:

```
:~$ set -- a b 'c d' e
:~$ for arg; do echo $arg; done
a
b
c d
e
```

### O loop 'for' no estilo C

Além da sintaxe comum, onde o controle das repetições é feito por uma lista de palavras, o `for` também pode ser controlado por uma expressão ternária, tal como sua contraparte na linguagem C. A sintaxe é um pouco diferente, mas é bem mais familiar para quem vem de outras linguagens:

```
# Em uma linha...

for ((EXP1; EXP2; EXP3)) { COMANDOS; }

for ((EXP1; EXP2; EXP3)); do COMANDOS; done

# Em várias linhas...

for ((EXP1; EXP2; EXP3)) {
    COMANDOS
}

for ((EXP1; EXP2; EXP3)); do
    COMANDOS
done
```

Desta forma, é possível percorrer os argumento a partir da expansão dos elementos nos parâmetros especiais `@` ou `*`, por exemplo:

```
:~$ set -- a b 'c d' e
:~$ for ((i = 1; i <= $#; i++)) { echo ${@:$i:1}; }
a
b
c d
e
```

Para entender como essa estrutura funciona, nós teremos que pensar em etapas:

**Primeira etapa:**

A expressão `EXP1`, que geralmente é a atribuição de um valor inicial a uma variável de referência, é avaliada: esta avaliação ocorre apenas uma vez,
antes do primeiro ciclo.

No exemplo, nós atribuímos o valor `1` à variável `i`:

```
for ((i = 1; i <= $#; i++)) { echo ${@:$i:1}; }
      -----
        ↑
```

**Segunda etapa:**

A expressão `EXP2` é avaliada quanto à verdade da sua afirmação. Geralmente, é uma comparação entre o valor na variável de referência e um valor inteiro utilizado como limite: se `EXP2` for avaliada como verdadeira, haverá um ciclo de execução do bloco de comandos; caso seja avaliada como falsa, nenhum novo ciclo será executado e o `for` será encerrado.

No exemplo, nós comparamos o valor corrente de `i` com o número total de argumentos no parâmetro especial `#`:

```
for ((i = 1; i <= $#; i++)) { echo ${@:$i:1}; }
             -------
                ↑
```

**Terceira etapa:**

Após executar um ciclo do bloco de comandos, a expressão em `EXP3`, que costuma ser uma alteração no valor da variável de referência, será avaliada.

No caso do exemplo, nós incrementamos em uma unidade o valor de `i`:

```
for ((i = 1; i <= $#; i++)) { echo ${@:$i:1}; }
                      ---
                       ↑
```

**Quarta etapa:**

Completadas as etapas anteriores, `EXP2` é novamente avaliada para confirmar se a sua afirmação ainda é verdadeira: se for, o bloco de comandos é executado novamente e `EXP3` é avaliada; caso contrário, nenhum novo ciclo é executado e o `for` é encerrado.

Apesar de bastante prático, o estilo C não é portável. Se isso for um requisito, nossa única opção será trabalhar com a expansão de uma lista de palavras. Mas, considerando que estamos escrevendo scripts para o shell interativo padrão de sistemas GNU/Linux, vale a pena saber como implementar a mesma técnica com os loops `while` e `until`.

### Expandindo parâmetros com os loops 'while' e 'until'

Observe o exemplo com o loop `while`:

```
:~$ set -- a b 'c d' e
:~$ i=1; while [[ $i -le $# ]]; do echo ${@:$i:1}; ((i++)); done
a
b
c d
e
```

O comando composto `while` avalia o estado de saída de um comando para permitir ou não a execução de um ciclo do bloco de comandos:

```
# Em uma linha...

while COMANDO_TESTADO; do COMANDOS; done

# Em várias linhas...

while COMANDO_TESTADO; do
    COMANDOS
done
```

No nosso exemplo, COMANDO_TESTADO é o comando composto `[[`, que avalia expressões assertivas: no caso, nós estamos afirmando que o valor corrente de `i` é menor ou igual ao número de argumentos no parâmetro especial `#`:

```
:~$ i=1; while [[ $i -le $# ]]; do echo ${@:$i:1}; ((i++)); done
               ---------------
                      ↑
```

A variável `i` foi iniciada com o valor `1` antes do `while` ser executado, o que fez com que a avaliação da expressão resultasse em verdadeiro e o comando `[[` terminasse com estado de sucesso. Sendo assim, o bloco de comandos é executado: primeiro, expandindo o parâmetro de número `1`:

```
:~$ i=1; while [[ $i -le $# ]]; do echo ${@:$i:1}; ((i++)); done
                                        ---------
                                            ↑
```

Em seguida, incrementando o valor em `i`:

```
:~$ i=1; while [[ $i -le $# ]]; do echo ${@:$i:1}; ((i++)); done
                                                   -------
                                                      ↑
```

Os ciclos continuarão até que o valor em `i` seja maior do que a quantidade de argumentos passados para o script e o comando `[[` termine com estado de erro.

Se optássemos pelo loop `until`, a técnica seria a mesma, com a diferença de que o comando testado precisa terminar com erro para que o bloco de comandos seja executado.

Portanto, afirmando que o valor de `i` é maior do que a quantidade de argumentos, nós temos a condição de erro necessária para a execução do bloco de comandos:

```
:~$ set -- a b 'c d' e
:~$ i=1; until [[ $i -gt $# ]]; do echo ${@:$i:1}; ((i++)); done
a
b
c d
e
```

