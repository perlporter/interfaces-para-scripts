# Curso de interfaces para scripts em shell

Repositório do conteúdo produzido para o curso de interfaces para scripts em shell.

## Links importantes

- [Sala das aulas ao vivo (Jit.si) - aberta às quartas, 19h UTC-3](https://meet.jit.si/SalaCursoISS)
- [Índice das aulas (apostilas e vídeos)](https://codeberg.org/blau_araujo/interfaces-para-scripts/src/branch/main/aulas/README.md)
- [Playlist das aulas no Youtube](https://youtube.com/playlist?list=PLXoSGejyuQGq4Zy0WFL0SU0AKP4e1Yc0c)
- [Vídeos e textos para aprender o shell do GNU/Linux](https://codeberg.org/blau_araujo/para-aprender-shell)
- [Dúvidas sobre os tópicos do curso (issues)](https://codeberg.org/blau_araujo/interfaces-para-scripts/issues)
- [Discussões sobre os tópicos de todos os nossos cursos (Gitter)](https://gitter.im/blau_araujo/community)

## Formas de apoio

* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* Doações via PIX: pix@blauaraujo.com
* [Versão impressa do Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/2022/02/17/versao-impressa-do-pequeno-manual-do-programador-gnu-bash/)